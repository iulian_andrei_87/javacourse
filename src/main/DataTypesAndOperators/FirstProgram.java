package main.DataTypesAndOperators;

/**
 * The class contains methods for:
 * 2. Tipuri de date si operatori
 * 2.1 Primul Program
 *
 * @author Iulian Andrei
 * @version 21.05.2017
 */
public class FirstProgram {

    /**
     * Exercise 2.1.1
     */
    private void showStars() {
        System.out.println("*****");
        System.out.println("*****");
        System.out.println("*****");
        System.out.println("*****");
    }

    /**
     * Exercise 2.1.2
     */
    private void showLetter() {
        System.out.println("+-------------------------------------+");
        System.out.println("|                                 ####|");
        System.out.println("|                                 ####|");
        System.out.println("|                                 ####|");
        System.out.println("|                                     |");
        System.out.println("|                                     |");
        System.out.println("|             Bill Gates              |");
        System.out.println("|             1 Microsoft Way         |");
        System.out.println("|             Redmond, WA 98104       |");
        System.out.println("|                                     |");
        System.out.println("+-------------------------------------+");
    }

    /**
     * Exercise 2.1.2
     */
    private void showBigInitials() {
        System.out.println("For the name Iulian Andrei...");
        System.out.println("");
        System.out.println("IIIII          A");
        System.out.println("  I           A A");
        System.out.println("  I          A   A");
        System.out.println("  I         A     A");
        System.out.println("  I        AAAAAAAAA");
        System.out.println("  I       A         A");
        System.out.println("IIIII    A           A");
    }

    /**
     * Method will run all methods from the class
     */
    public void runFirstProgram() {
        System.out.println("Exercise 2.1.1");
        showStars();
        System.out.println("");
        System.out.println("Exercise 2.1.2");
        showLetter();
        System.out.println("");
        System.out.println("Exercise 2.1.2");
        showBigInitials();
        System.out.println("");
    }
}
