package main.DataTypesAndOperators;

import java.util.Scanner;

/**
 * The class contains methods for:
 * 2. Tipuri de date si operatori
 * 2.4 Tipuri de date primitive în Java
 *
 * @author Iulian Andrei
 * @version 22.05.2017
 */
public class Primitives {

    private final Scanner scanner = new Scanner(System.in);

    private boolean readIsCapital() {
        System.out.println("Is the city a capital? (y/n)");
        String answer = scanner.nextLine();

        return answer.equals("y");
    }

    private int readNumberOfCitizen() {
        System.out.println("What is the number of citizens of the city?");
        return scanner.nextInt();
    }

    private int readIncomePerCitizen() {
        System.out.println("What is the average yearly income per citizen of the city?");
        return scanner.nextInt();
    }

    private void capital(boolean isCapital, int numberOfCitizen, int incomePerCitizen) {
        if ((isCapital && numberOfCitizen > 100000) || (numberOfCitizen > 200000 && incomePerCitizen > 72000)) {
            System.out.println("The city is a metropolis");
        } else {
            System.out.println("The city is not a metropolis");
        }
    }

    /**
     * Method will run all methods from the class
     */
    public void primitives() {
        System.out.println("Exercise 2.4.1");
        capital(readIsCapital(), readNumberOfCitizen(), readIncomePerCitizen());
        System.out.println("");
        System.out.println("Exercise 2.4.2");
//        showLetter();
        System.out.println("");
        System.out.println("Exercise 2.4.3");
//        showBigInitials();
        System.out.println("");
    }
}
