package main.DataTypesAndOperators;

/**
 * The class contains methods for:
 * 2. Tipuri de date si operatori
 * 2.3 Variabile. Identificatori. Literali
 *
 * @author Iulian Andrei
 * @version 21.05.2017
 */
public class Variables {

    /**
     * Exercise 2.3.1
     */
    private void showWithVariables() {
        int roomNr = 113;
        double closeTo = 2.71828;
        String learning = "Computer Science";

        System.out.println("This is room #" + roomNr);
        System.out.println("e is close to " + closeTo);
        System.out.println("I am learging a bit about " + learning);
    }


    /**
     * Exercise 2.3.2
     */
    private void showTwoVariables() {
        int birthYear = 1987;
        String nume = "Iulian Andrei";

        System.out.println("Mă numesc " + nume + " şi m-am născut în anul " + birthYear + ".");
    }

    /**
     * Exercise 2.3.3
     */
    private void schoolSchdule() {

        List<String> teachers = Arrays.asList(
                "Ms.Lapan",
                "Mrs. Gideon",
                "Mr. Davis",
                "Ms. Palmer",
                "Ms. Garcia",
                "Mrs. Barnett",
                "Ms. Johannessen",
                "Mr. James"
        );


        List<String> hours = Arrays.asList(
                "English III",
                "Precalculus",
                "Music Theory",
                "Biotechnology",
                "Principles of Technology I",
                "Latin II",
                "AP US History",
                "Business Computer Information Systems"
        );

        int i = 1;

        System.out.println("+----------------------------------------------------------------+");
        System.out.println("|" + i + "|                                 " + hours.get(0) + "|         " + teachers.get(0) + "|");
        System.out.println("|" + (i++) + "|                                 " + hours.get(1) + "|      " + teachers.get(1) + "|");
        System.out.println("|" + (i++) + "|                                " + hours.get(2) + "|        " + teachers.get(2) + "|");
        System.out.println("|" + (i++) + "|                               " + hours.get(3) + "|       " + teachers.get(3) + "|");
        System.out.println("|" + (i++) + "|                  " + hours.get(4) + "|       " + teachers.get(4) + "|");
        System.out.println("|" + (i++) + "|                                    " + hours.get(5) + "|     " + teachers.get(5) + "|");
        System.out.println("|" + (i++) + "|                               " + hours.get(6) + "|  " + teachers.get(6) + "|");
        System.out.println("|" + (i++) + "|       " + hours.get(7) + "|        " + teachers.get(7) + "|");
        System.out.println("+----------------------------------------------------------------+");
    }

    /**
     * Method will run all methods from the class
     */
    public void runVariables() {
        System.out.println("Exercise 2.3.1");
        showWithVariables();
        System.out.println("");
        System.out.println("Exercise 2.3.2");
        showTwoVariables();
        System.out.println("");
        System.out.println("Exercise 2.3.3");
        schoolSchdule();
        System.out.println("");
    }
}
