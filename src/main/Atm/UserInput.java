package main.Atm;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by iulianandrei on 22/05/2017.
 */
public class UserInput {

    private Scanner scan = new Scanner(System.in);

    public String username() {
        System.out.println("Username:");

        return scan.next();
    }

    public Integer password() {
        int password = 0;
        int i = 0;
        do {
            boolean error = false;

            try {
                System.out.println("Password:");
                password = new Scanner(System.in).nextInt();
            } catch (InputMismatchException e) {
                System.out.println("");
                if (i < 2) {
                    System.out.println("Password must be numeric! Please try again.");
                    error = true;
                } else {
                    System.out.println("Three tries completed! Account is blocked.");
                    error = true;
                    password = 0;
                }
                System.out.println("");
            }

            if (String.valueOf(password).length() != 4 && !error) {
                if (i < 2) {
                    System.out.println("Password must have exactly 4 characters! Please try again.");
                } else {
                    System.out.println("Three tries completed! Account is blocked.");
                    password = 0;
                }
                System.out.println("");
            }

            i++;
        } while (String.valueOf(password).length() != 4 && i < 3);

        if (password == 0) {
            return null;
        }

        return password;
    }

    public Integer chooseOperation() {
        System.out.println("Choose operation:");
        System.out.println("1. Deposit");
        System.out.println("2. Withdraw");
        System.out.println("3. Check account balance");
        System.out.println("4. Other operation");

        int selection = 0;

        do {
            try {
                selection = scan.nextInt();
            } catch (RuntimeException e) {
                System.out.println("Selection is not valid! Please try again.");
            }
        } while (selection < 1 && selection > 4);

        return selection;
    }
}
