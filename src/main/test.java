package main;


public class test {

    private static final Scanner scan = new Scanner(System.in);

    /**
     * Main method for running methos
     *
     * @param args String
     */
    public static void main(String[] args) {
        System.out.println("Insert test string: ");
        String newString = scan.nextLine();

        boolean var = switchCase(newString);
        if (var) {
            System.out.println("test is correct");
        } else {
            System.out.println("test is not correct");
        }

        System.out.println("Insert a number between 0-5");
        int intVar = scan.nextInt();
        whileCase(intVar);

        doWhileCase(new Scanner(System.in));
        Random rand = new Random();
        justAdd(new Scanner(System.in), rand.nextInt(100));
    }

    private static boolean switchCase(String var) {
        switch (var) {
            case "test":
                return true;
            case "no test":
                return true;
            default:
                return false;
        }
    }

    private static void whileCase(int intVar) {

        while (intVar <= 10) {
            System.out.println("Var needs to reach 10. Right now at " + intVar);
            System.out.println("");
            intVar++;
        }
    }

    private static void doWhileCase(Scanner scan) {
        Random rand = new Random();
        System.out.println("Guess between orange, blue or yellow to stop the loop: ");
        String correctColor;
        int i = 0;
        int limit = 10;
        do {
            if (i != 0) {
                System.out.println("Guess between orange, blue or yellow to stop the loop: ");
            }
            int randNumber = rand.nextInt(2);
            if (randNumber == 0) {
                correctColor = "orange";
            } else if (randNumber == 1) {
                correctColor = "blue";
            } else {
                correctColor = "yellow";
            }

            String inputColor = scan.nextLine();
            if (correctColor.equals(inputColor)) {
                System.out.println("You are right");
                System.out.println("");
                break;
            }

            i++;
            if (i == 10) {
                System.out.println("You are wrong. You have no more tries left.");
                System.out.println("");
            } else {
                System.out.println("You are wrong. You have " + (limit - i) + " more tries");
                System.out.println("");
            }
        } while (i < limit);
    }

    private static void justAdd(Scanner scan, int limit) {
        System.out.println("Insert a random number from 0 to " + limit + ":");
        int intVal = scan.nextInt();

        if (intVal < limit) {
            for (int i = intVal; i <= limit; i++) {
                System.out.println("Started at " + intVal + ". Current number is at " + i + " from " + limit);
                System.out.println("");
            }
        } else {
            System.out.println("Value is above " + limit + "... nothing to do.");
            System.out.println("");
        }
    }

    public static void test() {
        String[] test = new String[10];

        String intString;
        for (int i = 0; i < test.length; i++) {
//            (String) test[i] = (String)rand.nextInt(100);
        }

        test[0] = "test";

        String newString = test[0];
        System.out.println(newString);


        // reminder
        // for ( {type-ul elementelor} {nume variabila}: {array} {
        //      operatiuni pe variabila
        // }

        // reminder arrayList
//        ArrayList<String> newArrList = new ArrayList<String>();
//        newArrList.add("test");
//        newArrList.remove(0);
        // reminder
        // la ArrayList folosim clase wrapper int/Integer char/Character String double/Double
//        Collection testTest = new ArrayList<Integer>(100);
//        testTest.add(4);
//        testTest.add(5);
//        testTest.add(10);
//
//        Iterator<Integer> iterator = testTest.iterator();
//
//        testTest.remove(new Integer(4)); // stergere pe obiect
//        testTest.remove(0); //stergere pe index
//        testTest.indexOf(4); //cauta indexul valorii 4, daca nu gaseste nimic returneaza -1
//        testTest.clear(); //sterge toate elementele din arrray
//        testTest.trimToSize(); //sterge capacitatea totala la cea folosita
//        testTest.isEmpty(); //returneaza boolean
//        Collections.shuffle(testTest);
//        Collections.sort(testTest);


//        for (int i : testTest) {
//            System.out.println(testTest.get(i));
//        }

//        ArrayList<String> nestedArray = new ArrayList<String>();

//        System.out.println(testTest.get(0));
//
//        Integer i = new Integer(7);
//        Integer i2 = 7;
//        int i3 = i;
//
//        System.out.println(i3);
//
//
//
    }


//    1. Schimbaţi valorile iniţiale ale variabilelor astfel încât să nu fie afişat doar un singur mesaj. Adăugaţi if-uri astfel încât programul să afişeze minim 3 mesaje. Adăugaţi comentarii. Ştergeţi acoladele şi observaţi diferenţele.


}