package main;

import main.DataTypesAndOperators.FirstProgram;
import main.DataTypesAndOperators.Variables;

/**
 * This class is used to run methods designed for the Jademy Java course
 *
 * @author Iulian Andrei
 * @version 21.05.2017
 */
public class RunMethods {

    /**
     * Main method for running methos
     *
     * @param args String
     */
    public static void main(String[] args) {
        new FirstProgram().runFirstProgram();
        new Variables().runVariables();
    }
}
