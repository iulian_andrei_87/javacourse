package main.PetShop.Controllers;

import main.PetShop.Models.Cart;
import main.PetShop.Models.ShopDataBase;

import java.util.Scanner;

/**
 * Class is an abstract for controllers
 */
abstract class AbstractController {

    protected ShopDataBase dataBase;

    protected Cart cart;

    protected AbstractController(ShopDataBase dataBase, Cart cart) {
        this.dataBase = dataBase;
        this.cart = cart;
    }

    /**
     * Method is an abstract for the new controllers
     */
    abstract void runController();

    /**
     * Method gets int input from user
     *
     * @return int
     */
    // FIXME fa undeva un Scanner static care sa il foloseasca toti - vezi Design Pattern Singleton
    // FIXME metoda poata sa aiba si logica - sa introduci intre anumite numere - le dai ca parametrii si faci un while inauntru care sa citeasca de mai multe ori
    // FIXME  daca crezi ca ai nevoie
    // FIXME si asta ar putea fi o metoda utilitara
    public int selection() {
        Scanner scan = new Scanner(System.in);
        return scan.nextInt();
    }
}
