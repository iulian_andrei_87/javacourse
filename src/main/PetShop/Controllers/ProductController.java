package main.PetShop.Controllers;

import main.PetShop.Models.*;
import main.PetShop.RunShop;
import main.PetShop.Util.ProductUtil;
import main.PetShop.Views.MenuView;
import main.PetShop.Views.ProductView;

import java.util.ArrayList;

/**
 * Class holds controllers for the products menu
 */
// FIXME o parte din logica de prin modele ar trebui sa ajunga prin controllere
public class ProductController extends AbstractController {
    /**
     * constant that holds the ProductUtil object
     */
    private final ProductUtil helper;

    public ProductController(ShopDataBase dataBase, Cart cart) {
        super(dataBase, cart);
        helper = new ProductUtil();
    }

    /**
     * Method runs the main controller for the class
     */
    public void runController() {
        // local variable that holds the products from the database
        ArrayList<Product> products = super.dataBase.getProducts();

        do {
            // instantiate new menu and display it
            MenuView menu = new MenuView("Products for animal", helper.processMenuItems(products));
            menu.showView();

            // grab new selection
            int selection = this.selection();

            if (selection == RunShop.RETURN_TO_MENU) { //break if return to menu selected
                break;
            } else { // display submenu
                runSubMenu(products, selection, super.dataBase, super.cart);
            }
        } while (true);
    }

    /**
     * Method that controls the products submenu view and the available options
     *
     * @param products  ArrayList<Product>
     * @param selection int
     * @param dataBase  ShopDataBase
     * @param cart      Cart
     */
    private void runSubMenu(ArrayList<Product> products, int selection, ShopDataBase dataBase, Cart cart) {
        // local variable that holds the products from the database
        ArrayList<Product> subMenuProducts = helper.processSubMenuItems(selection, products, dataBase);

        do {
            // instantiate new menu and display it
            MenuView menu = new MenuView("Products for " + subMenuProducts.get(0).getProductSubCategory(), helper.getSubMenuTitles(subMenuProducts));
            menu.showView();

            // grab new selection
            int newSelection = this.selection();

            if (newSelection == RunShop.RETURN_TO_MENU) { //break if return to menu selected
                break;
            } else {
                // display product page
                runProductPage(subMenuProducts.get(newSelection - 1), cart);
            }
        } while (true);
    }

    /**
     * Method that controls the product view and the available options
     *
     * @param selectedProduct Product
     * @param cart            Cart
     */
    private void runProductPage(Product selectedProduct, Cart cart) {
        // initialize new product view and display it
        new ProductView(selectedProduct).showView();

        // grab new selection
        int selection = this.selection();

        if (selection != RunShop.RETURN_TO_MENU) {
            cart.addToCart(selectedProduct);
        }
    }
}
