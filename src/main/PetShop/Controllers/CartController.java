package main.PetShop.Controllers;

import main.PetShop.Models.*;
import main.PetShop.RunShop;
import main.PetShop.Views.CartView;

import java.util.ArrayList;

/**
 * Class that handles all the cart decisions
 */
public class CartController extends AbstractController {
    /**
     * Class variable that holds the cartView
     */
    private final CartView cartView;

    /**
     * Class constructor
     *
     * @param dataBase ShopDataBase
     * @param cart     Cart
     */
    public CartController(ShopDataBase dataBase, Cart cart) {
        super(dataBase, cart);
        this.cartView = new CartView(cart);
    }

    /**
     * Method that handle the main operations of the cart
     */
    public void runController() {
        // main controller
        boolean flag = true;
        do {

            // display cart view
            cartView.showView();

            // set selector
            int selection = this.selection();

            switch (selection) {
                case RunShop.RETURN_TO_MENU:
                    flag = false;
                    break;
                case 1:
                    updateQuantityController();
                    continue;
                case 2:
                    removeProductFromCartController();
                    continue;
                case 3:
                    if (clearCartController()) {
                        break;
                    } else {
                        continue;
                    }
                case 4:
                    cart.finalizeOrder(dataBase);
                    break;
            }
        } while (flag);
    }

    /**
     * Controller for update quantity controller
     */
    private void updateQuantityController() {
        // set local variables from global
        ArrayList<Product> cartProducts = super.cart.getCartProducts();
        ArrayList<Integer> cartProductQuantities = super.cart.getCartQuantities();

        // display update product view
//        cartView.showUpdateProductView(cartProducts, cartProductQuantities);

        // initiate selector
        int productToUpdate = selection();

        // set flag
        boolean flag;

        do {
//            cartView.showUpdateQuantityView(cartProducts, cartProductQuantities);
            int quantityToUpdate = selection();
            flag = !cart.updateQuantity(quantityToUpdate, cartProducts.get(productToUpdate - 1));
        } while (flag);
    }

    /**
     * Controller for the remove product from cart functionality
     */
    private void removeProductFromCartController() {
        // set flag
        boolean flag;

        do {
//            cartView.showRemoveFromCartView(super.cart.getCartProducts(), super.cart.getCartQuantities());
            flag = !super.cart.removeProductFromCart(selection());
        } while (flag);
    }

    /**
     * Controller for the clear cart functionality
     *
     * @return boolean
     */
    private boolean clearCartController() {
        // show clear cart view
//        cartView.showClearCartView(super.cart.getCartProducts(), super.cart.getCartQuantities());

        // set selector
        int selection = selection();

        // return base on selection
        if (selection == 1) {
            super.cart.clearCart();
            return true;
        }

        return false;
    }
}
