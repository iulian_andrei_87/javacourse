package main.PetShop.Util;

import main.PetShop.Models.Product;

import java.util.ArrayList;

/**
 * Class contains methods that help process cart data
 */
// FIXME mai poarta denumirea de clase de utilitati, dar asta parca ar fi ceva pentru cart
// FIXME daca faci un model cart item, poti calcula in el pretul final pentru un produs, apoi ai un array pe care faci o suma in Cart
public class CartUtil {
    /**
     * Method that calculates the total price of multiple products
     *
     * @param cartProducts   ArrayList<Product>
     * @param cartQuantities ArrayList<Integer>
     * @return double
     */
    public static double processTotalPrice(ArrayList<Product> cartProducts, ArrayList<Integer> cartQuantities) {
        double totalPrice = 0;

        for (int i = 0; i < cartProducts.size(); i++) {
            double price = cartProducts.get(i).getProductPrice() * cartQuantities.get(i);

            totalPrice += price;
        }

        return totalPrice;
    }
}
