package main.PetShop.Util;

import main.PetShop.Models.Product;
import main.PetShop.Models.ShopDataBase;

import java.util.ArrayList;

/**
 * Class contains methods that help process product data
 */
// FIXME par metode pentru baza de date
public class ProductUtil {
    /**
     * Method that updates the product quantity
     *
     * @param product  Product
     * @param quantity int
     */
    public static void updateProductQuantity(Product product, int quantity) {
        product.setProductQuantity(product.getProductQuantity() - quantity);
    }

    /**
     * Method get menu items from products list
     *
     * @return ArrayList
     */
    public ArrayList<String> processMenuItems(ArrayList<Product> products) {
        // set local variables to use in the method
        ArrayList<String> menuItems = new ArrayList<>();

        // get menu items from the existing products
        for (Product prod : products) {
            if (prod.getProductSubCategory() == null && prod.getProductCategory().equals("Animal")) {
                menuItems.add(prod.getProductName());
            }
        }

        return menuItems;
    }

    /**
     * Method processes the items of the submenu
     *
     * @param selection int
     * @param products  ArrayList
     * @param dataBase  ShopDataBase
     * @return ArrayList
     */
    public ArrayList<Product> processSubMenuItems(int selection, ArrayList<Product> products, ShopDataBase dataBase) {
        // set local variables to use in the method
        String selectedProductSubCategory = dataBase.getProductSubcategory().get(selection - 1);
        ArrayList<Product> menuProducts = new ArrayList<>();

        for (Product prod : products) {
            // add menu items and products if products have a subcategory
            if (prod.getProductSubCategory() == null && prod.getProductName().equals(selectedProductSubCategory)) {
                menuProducts.add(prod);
                // add menu items and product if products don't have a subcategory
            } else if (prod.getProductSubCategory() != null && prod.getProductSubCategory().equals(selectedProductSubCategory)) {
                menuProducts.add(prod);
            }
        }

        return menuProducts;
    }

    /**
     * Method gets menu items names from products
     *
     * @param products ArrayList
     * @return ArrayList
     */
    public ArrayList<String> getSubMenuTitles(ArrayList<Product> products) {
        ArrayList<String> ProductsTitles = new ArrayList<>();

        for (Product product : products) {
            ProductsTitles.add(product.getProductName());
        }

        return ProductsTitles;
    }
}
