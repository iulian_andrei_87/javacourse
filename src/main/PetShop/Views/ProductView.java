package main.PetShop.Views;

import main.PetShop.Models.Product;

/**
 * Class that displays the product view
 */
public class ProductView extends AbstractView {
    /**
     * Class variable that holds the product name
     */
    private String name;

    /**
     * Class variable that holds the product quantity
     */
    private int quantity;

    /**
     * Class variable that holds the product price
     */
    private double price;

    /**
     * Implicit constructor
     */
    public ProductView() {
    }

    /**
     * Class constructor
     *
     * @param product Product
     */
    // FIXME poate e mai bine sa ai un camp de product, si aici sa ai diverse metode de afisare a lui
    // FIXME sau sa o faci clasa de utilitati (metode statice) cu diverse metode care iau ca parametru un Product si afiseaza ce ai nevoie din el
    // FIXME    Product cu denumire si pret, sau doar product, sau in orice alt fel
    public ProductView(Product product) {
        this.name = product.getProductName();
        this.quantity = product.getProductQuantity();
        this.price = product.getProductPrice();
    }

    /**
     * Method that displays the product view
     */
    public void showView() {
        System.out.println("\n" +
                "Product name: " + this.name + "\n" +
                "Product price: " + this.price + "\n" +
                "Current quantity: " + this.quantity + "\n");

        System.out.println("1. Add product to cart");
        endOfMenu();
    }
}
