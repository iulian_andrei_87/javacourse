package main.PetShop.Views;

import java.util.ArrayList;

/**
 * Class that displays the main menu
 */
public class MenuView extends AbstractView {
    /**
     * Class variable used for the title of the screen
     */
    private String title;

    /**
     * Class variable used for the elements of the screen
     */
    private ArrayList<String> menuElements = new ArrayList<>();

    /**
     * Implicit constructor
     */
    public MenuView() {
    }

    /**
     * Class constructor
     *
     * @param title        String
     * @param menuElements ArrayList
     */
    public MenuView(String title, ArrayList<String> menuElements) {
        this.title = title;
        this.menuElements.addAll(menuElements);
    }

    /**
     * Method that displays the main menu
     */
    public void showView() {
        System.out.println(this.title);
        System.out.println("");

        for (int i = 0; i < this.menuElements.size(); i++) {
            int menuSelector = i + 1;
            System.out.println(menuSelector + ". " + this.menuElements.get(i));
        }

        endOfMenu();
    }
}
