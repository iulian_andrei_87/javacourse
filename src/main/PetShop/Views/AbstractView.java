package main.PetShop.Views;

/**
 * Interface for the views
 */
abstract class AbstractView {

    /**
     * Method that should be present on all views
     */
    abstract void showView();

    /**
     * Method displays common menu throughout the app
     */
    protected void endOfMenu() {
        System.out.println("");
        System.out.println("0. Return to previous menu");
        System.out.println("");
        System.out.println("Please select desired option");
    }
}
