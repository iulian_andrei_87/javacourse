package main.PetShop.Views;

import main.PetShop.Models.Cart;
import main.PetShop.Models.Product;
import main.PetShop.Util.CartUtil;

import java.util.ArrayList;

/**
 * Class used for displaying the cart to the user
 */
public class CartView extends AbstractView {
    /**
     * Class variable that holds the cart data
     */
    private Cart cart;

    private ArrayList<Product> cartProducts;
    private ArrayList<Integer> cartQuantities;

    /**
     * Class constructor
     *
     * @param cart Cart
     */
    public CartView(Cart cart) {
        this.cart = cart;
        this.cartProducts = cart.getCartProducts();
        this.cartQuantities = cart.getCartQuantities();
    }

    /**
     * View displays the cart static content
     */
    public void showView() {
        if (cart.getCartProducts().size() == 0) {
            System.out.println("");
            System.out.println("Cart is empty");
            System.out.println("====================");
            System.out.println("");
        } else {
            System.out.println("");
            System.out.println("Cart");
            System.out.println("====================");
            System.out.println("");

            showProducts();
            showMenuItems();
        }

        endOfMenu();
    }

    /**
     * Method displays the dynamic part of the cart
     */
    private void showProducts() {
        System.out.println("");
        for (int i = 0; i < cartProducts.size(); i++) {

            String category;

            if (cartProducts.get(i).getProductSubCategory() == null) {
                category = cartProducts.get(i).getProductCategory();
            } else {
                category = cartProducts.get(i).getProductSubCategory();
            }

            int menuSelector = i + 1;

            System.out.println(menuSelector + ". " + cartProducts.get(i).getProductName() + "     Category: " + category + "     Quantity: " + cartQuantities.get(i) + "     Price: " + cartProducts.get(i).getProductPrice());
        }

        System.out.println("TOTAL: " + CartUtil.processTotalPrice(cartProducts, cartQuantities));
    }

    /**
     * Methods gets all menu items and displays them
     */
    private void showMenuItems() {
        ArrayList<String> cartMenuItems = cart.getCartMenuItems();
        System.out.println("");

        // iterates through all elements of the menu items and displays them
        for (int i = 0; i < cartMenuItems.size(); i++) {
            int menuSelector = i + 1;

            System.out.println(menuSelector + ". " + cartMenuItems.get(i));
        }
    }

    /**
     * View shows full cart and update product message
     */
    public void showUpdateProductView() {
        showProducts();
        System.out.println("\n Select product to update quantity:");
    }

    /**
     * View shows full cart and update quantity message
     */
    public void showUpdateQuantityView() {
        showProducts();
        System.out.println("\n Update quantity:");
    }

    /**
     * View shows full cart and remove from cart message
     */
    public void showRemoveFromCartView() {
        showProducts();
        System.out.println("\n Select product to remove:");
    }

    /**
     * View shows full cart and clear cart message
     */
    public void showClearCartView() {
        showProducts();
        System.out.println(
                "\n Are you sure you want to delete all products from cart?: \n" +
                        "1. Yes \n" +
                        "2. No"
        );
    }
}
