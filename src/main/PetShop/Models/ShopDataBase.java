package main.PetShop.Models;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class with content that simulates a database
 */
public class ShopDataBase {
    /**
     * Class variable that holds the initialized products
     */
    private static ArrayList<Product> products = new ArrayList<>();

    /**
     * Class variable that holds the main menu items
     */
    private final List<String> menuItems = Arrays.asList(new String[]{"Products", "Cart", "About"});
    // FIXME FYI blocurile de initializare se executa inaintea constructorului, nu pot sa zic ca le recomand
    // FIXME - incearca private final List<String> menuItems = Arrays.asList(new String[]{"...","..."});
    // FIXME unde List IS-A ArrayList
//    {
//        menuItems = new ArrayList<>();
//        menuItems.add("Products");
//        menuItems.add("Cart");
//        menuItems.add("About");
//    }

    /**
     * Class variable that holds the product subcategories
     */
    private final ArrayList<String> PRODUCT_SUBCATEGORY;

    // FIXME seamana a enum :)
    {
        PRODUCT_SUBCATEGORY = new ArrayList<>();
        PRODUCT_SUBCATEGORY.add("Dog");
        PRODUCT_SUBCATEGORY.add("Cat");
        PRODUCT_SUBCATEGORY.add("Hamster");
        PRODUCT_SUBCATEGORY.add("Dinosaur");
        PRODUCT_SUBCATEGORY.add("Dragon");
        PRODUCT_SUBCATEGORY.add(null);
    }

    /**
     * Class variable that holds the product categories
     */
    private final ArrayList<String> PRODUCT_CATEGORY;

    {
        PRODUCT_CATEGORY = new ArrayList<String>();
        PRODUCT_CATEGORY.add("Animal");
        PRODUCT_CATEGORY.add("Food");
        PRODUCT_CATEGORY.add("Accessory");
        PRODUCT_CATEGORY.add("Service");
    }

    /**
     * Constant that holds the cart menu items TODO: move into database
     */
    private final ArrayList<String> cartMenuItems;

    // FIXME cred ca tin mai mult de logica/menu decat de baza de date unde stochezi - inserezi, modifici, stergi informatii
    // FIXME niste variabile statice sau enum care sa indice optiunile astea, dar undeva prin view
    {
        cartMenuItems = new ArrayList<>();
        cartMenuItems.add("Update quantity");
        cartMenuItems.add("Remove product from cart");
        cartMenuItems.add("Clear cart");
        cartMenuItems.add("Purchase products");
    }

    /**
     * Product getter
     *
     * @return ArrayList<Product>
     */
    public ArrayList<Product> getProducts() {
        return products;
    }

    /**
     * Product categories getter
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> getProductCategories() {
        return PRODUCT_CATEGORY;
    }

    /**
     * Product subcategories getter
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> getProductSubcategory() {
        return PRODUCT_SUBCATEGORY;
    }

    /**
     * Menu items getter
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> getMenuItems() {
        return menuItems;
    }

    /**
     * Getter for cartMenuItems
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> getCartMenuItems() {
        return cartMenuItems;
    }

    /**
     * Method that creates products objects and sets them to the class variable products
     */
    // FIXME - vezi DAO - baza de date poate sa aibe metode de cautare dupa index, dupa categorie, dupa pret, etc si sa intoarca doar acele produse
    // FIXME sau metode de modificare/stergere/inserare a unui produs; acestea sunt operatii specifice pentru clasa asta
    public void initializeProducts() {
        // initialize dog food products
        products.add(new Product(PRODUCT_CATEGORY.get(1), PRODUCT_SUBCATEGORY.get(0), "Dry Food", 50, 50));
        products.add(new Product(PRODUCT_CATEGORY.get(1), PRODUCT_SUBCATEGORY.get(0), "Canned Food", 30, 30));

        // initialize cat food products
        products.add(new Product(PRODUCT_CATEGORY.get(1), PRODUCT_SUBCATEGORY.get(1), "Dry Food", 50, 50));
        products.add(new Product(PRODUCT_CATEGORY.get(1), PRODUCT_SUBCATEGORY.get(1), "Canned Food", 30, 30));

        // initialize hamster food products
        products.add(new Product(PRODUCT_CATEGORY.get(1), PRODUCT_SUBCATEGORY.get(2), "Dry Food", 100, 30));
        products.add(new Product(PRODUCT_CATEGORY.get(1), PRODUCT_SUBCATEGORY.get(2), "Canned Food", 50, 10));

        // initialize dinosaur food products
        products.add(new Product(PRODUCT_CATEGORY.get(1), PRODUCT_SUBCATEGORY.get(3), "Dry Food", 20, 200));
        products.add(new Product(PRODUCT_CATEGORY.get(1), PRODUCT_SUBCATEGORY.get(3), "Canned Food", 10, 150));

        // initialize dragon food products
        products.add(new Product(PRODUCT_CATEGORY.get(1), PRODUCT_SUBCATEGORY.get(4), "Dry Food", 15, 300));
        products.add(new Product(PRODUCT_CATEGORY.get(1), PRODUCT_SUBCATEGORY.get(4), "Canned Food", 10, 250));

        // initialize accessory products
        products.add(new Product(PRODUCT_CATEGORY.get(2), PRODUCT_SUBCATEGORY.get(2), "Small Cage", 5, 100));
        products.add(new Product(PRODUCT_CATEGORY.get(2), PRODUCT_SUBCATEGORY.get(1), "Medium Cage", 5, 200));
        products.add(new Product(PRODUCT_CATEGORY.get(2), PRODUCT_SUBCATEGORY.get(0), "Medium Cage", 5, 250));
        products.add(new Product(PRODUCT_CATEGORY.get(2), PRODUCT_SUBCATEGORY.get(3), "Big Cage", 5, 550));
        products.add(new Product(PRODUCT_CATEGORY.get(2), PRODUCT_SUBCATEGORY.get(4), "Big Cage", 5, 550));

        // initialize services products
        products.add(new Product(PRODUCT_CATEGORY.get(3), PRODUCT_SUBCATEGORY.get(0), "Fur cutting", 100, 150));
        products.add(new Product(PRODUCT_CATEGORY.get(3), PRODUCT_SUBCATEGORY.get(1), "Fur cutting", 100, 150));
        products.add(new Product(PRODUCT_CATEGORY.get(3), PRODUCT_SUBCATEGORY.get(0), "Washing", 100, 150));
        products.add(new Product(PRODUCT_CATEGORY.get(3), PRODUCT_SUBCATEGORY.get(1), "Washing", 100, 150));
        products.add(new Product(PRODUCT_CATEGORY.get(3), PRODUCT_SUBCATEGORY.get(2), "Washing", 100, 50));
        products.add(new Product(PRODUCT_CATEGORY.get(3), PRODUCT_SUBCATEGORY.get(3), "Washing", 100, 350));
        products.add(new Product(PRODUCT_CATEGORY.get(3), PRODUCT_SUBCATEGORY.get(4), "Washing", 100, 350));
        products.add(new Product(PRODUCT_CATEGORY.get(3), PRODUCT_SUBCATEGORY.get(4), "Flame treatment", 100, 350));

        // initialize animal products
        products.add(new Product(PRODUCT_CATEGORY.get(0), PRODUCT_SUBCATEGORY.get(5), "Dog", 10, 800));
        products.add(new Product(PRODUCT_CATEGORY.get(0), PRODUCT_SUBCATEGORY.get(5), "Cat", 15, 500));
        products.add(new Product(PRODUCT_CATEGORY.get(0), PRODUCT_SUBCATEGORY.get(5), "Hamster", 25, 100));
        products.add(new Product(PRODUCT_CATEGORY.get(0), PRODUCT_SUBCATEGORY.get(5), "Dinosaur", 3, 2300));
        products.add(new Product(PRODUCT_CATEGORY.get(0), PRODUCT_SUBCATEGORY.get(5), "Dragon", 1, 5300));
    }
}
