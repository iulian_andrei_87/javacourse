package main.PetShop.Models;

/**
 * Class to use in order to create new products.
 */
// FIXME validari pe clasa pe setteri?, ex: pretul sa nu fie negativ, cantitatea sa nu fie negativa
// FIXME    unless te folosesti de valori negative cu un motiv, caz in care trebuie sa specifici in documentatie
public class Product {
    /**
     * Class variable that contains the product category
     */
    private String productCategory;

    /**
     * Class variable that contains the product subcategory
     */
    private String productSubCategory;

    /**
     * Class variable that contains the product name
     */
    private String productName;

    /**
     * Class variable that contains the product quantity
     */
    private int productQuantity;

    /**
     * Class variable that contains the product price
     */
    private double productPrice;

    /**
     * Class constructor
     *
     * @param productCategory    String
     * @param productSubCategory String
     * @param productName        String
     * @param productQuantity    int
     * @param productPrice       double
     */
    Product(
            String productCategory,
            String productSubCategory,
            String productName,
            int productQuantity,
            double productPrice
    ) {
        this.productCategory = productCategory;
        this.productSubCategory = productSubCategory;
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
    }

    /**
     * Implicit class constructor
     */
    public Product() {
    }

    /**
     * Method that sets the productCategory class variable
     *
     * @param productCategory String
     */
    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    /**
     * Method that returns the productCategory class variable
     *
     * @return String
     */
    public String getProductCategory() {
        return this.productCategory;
    }

    /**
     * Method that sets the productSubCategory class variable
     *
     * @param productSubCategory String
     */
    public void setProductSubCategory(String productSubCategory) {
        this.productSubCategory = productSubCategory;
    }

    /**
     * Method that returns the productSubCategory class variable
     *
     * @return String
     */
    public String getProductSubCategory() {
        return this.productSubCategory;
    }

    /**
     * Method that sets the productName class variable
     *
     * @param productName String
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Method that returns the productName class variable
     *
     * @return String
     */
    public String getProductName() {
        return this.productName;
    }

    /**
     * Method that sets the productQuantity class variable
     *
     * @param productQuantity int
     */
    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    /**
     * Method that returns the productQuantity class variable
     *
     * @return int
     */
    public int getProductQuantity() {
        return this.productQuantity;
    }

    /**
     * Method that sets the productPrice class variable
     *
     * @param productPrice double
     */
    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    /**
     * Method that returns the productPrice class variable
     *
     * @return double
     */
    public double getProductPrice() {
        return this.productPrice;
    }
}