package main.PetShop.Models;

import main.PetShop.Util.ProductUtil;

import java.util.ArrayList;

/**
 * Class that holds the cart
 */
// FIXME in timp o sa devina redundant sa le mai pui si cu model
public class Cart {
    // FIXME mai fa un model UserProduct care sa contina un Product si un Integer in loc sa ai doi array pe care sa ii asociezi
    /**
     * Class variable that holds the cart objects
     */
    private ArrayList<Product> userCart = new ArrayList<>();

    /**
     * Class variable that holds the cart quantities
     */
    private ArrayList<Integer> userCartQuantity = new ArrayList<>();

    // FIXME modelul pentru Cart trebuie sa fie independent pentru BD - vezi coeziune si cuplare
    private ShopDataBase dataBase;

    public Cart() {
    }

    public Cart(ShopDataBase dataBase) {
        this.dataBase = dataBase;
    }

    /**
     * Getter for cartProducts, if they exist
     *
     * @return ArrayList<Product>|null
     */
    public ArrayList<Product> getCartProducts() {
        if (userCart.size() < 0) {
            return null;
        }

        return userCart;
    }

    /**
     * Getter for cart quantities
     *
     * @return ArrayList<Integer>
     */
    public ArrayList<Integer> getCartQuantities() {
        return userCartQuantity;
    }


    /**
     * Method that adds product to user cart
     *
     * @param product Product
     */
    public void addToCart(Product product) {
        // FIXME toata logica asta o sa dispara daca scapi de array asociativi si faci o clasa
        // FIXME vezi si HashMap care face ceva asemanator cu logica de mai jos - doar ca mai simplu
        boolean updateFlag = false;

        if (userCart.size() > 0) {

            for (int i = 0; i < userCart.size(); i++) {
                if (userCart.get(i) == product) {
                    userCartQuantity.set(i, (userCartQuantity.get(i) + 1));
                    updateFlag = true;
                }
            }

            if (!updateFlag) {
                userCart.add(product);
                userCartQuantity.add(1);
            }
        } else {

            userCart.add(product);
            userCartQuantity.add(1);
        }
    }

    /**
     * Method updates the quantity inside of the cart
     *
     * @param quantityToUpdate int
     * @param productToUpdate  Product
     * @return boolean
     */
    public boolean updateQuantity(int quantityToUpdate, Product productToUpdate) {
        // FIXME again, too much logic de care ai putea scapa daca ai folosi HashMap sau inca un model
        // initialize variables for calculations
        int currentProductQuantity = 0;
        int productIndex = 0;

        // FIXME ai grija la formatarea codului
        // find product to update inside of the cart
        for (int i = 0; i < userCartQuantity.size(); i++) {
            if (productToUpdate == userCart.get(i)) {

                // set current quantity from the identified product
                currentProductQuantity = userCartQuantity.get(i);

                // set index
                productIndex = i;
            }
        }

        // return false if new quantity exceeds //TODO: check if, might be incorrect
        if ((productToUpdate.getProductQuantity() < quantityToUpdate) ||
                // return false if the new quantity results in smaller than 0
                ((productToUpdate.getProductQuantity() - quantityToUpdate) < 0) ||
                //return false if the new quantity exceeds the total quantity existing for the product
                ((currentProductQuantity + quantityToUpdate) > productToUpdate.getProductQuantity()) ||
                // return false if new quantity is smaller than 0
                ((currentProductQuantity + quantityToUpdate) < 0)) {
            return false;
        }

        // calculate and set new quantity
        int newQuantity = userCartQuantity.get(productIndex) + quantityToUpdate;
        userCartQuantity.set(productIndex, newQuantity);

        return true;
    }

    /**
     * Method that removes all the products from the cart
     */
    public void clearCart() {
        // clear arrays
        userCart.clear();
        userCartQuantity.clear();
    }

    /**
     * Method that removes a specific product from the cart using the cart index
     *
     * @param indexOfProduct int
     * @return boolean
     */
    public boolean removeProductFromCart(int indexOfProduct) {
        // adjust index in accordance with the selection logic
        indexOfProduct -= 1;

        // return false if the index is bigger than size of cart or bellow 0
        if (indexOfProduct < 0 || indexOfProduct > userCartQuantity.size()) {
            return false;
        }

        // remove cart and cart quantity for the index
        userCart.remove(indexOfProduct);
        userCartQuantity.remove(indexOfProduct);

        return true;
    }

    /**
     * Method removes product quantity from product
     *
     * @param dataBase ShopDataBase
     */
    // FIXME logica asta ar trebuie sa stea in alta parte (nu este ceva specific pentru Cart), plus ai un ShopDataBase si in constructor
    public void finalizeOrder(ShopDataBase dataBase) {
        ArrayList<Product> products = dataBase.getProducts();

        for (Product dbProd : products) {
            for (int i = 0; i < userCart.size(); i++) {
                if (dbProd == userCart.get(i)) {
                    ProductUtil.updateProductQuantity(userCart.get(i), userCartQuantity.get(i));
                }
            }
        }

        // clear cart after quantities are removed from db
        clearCart();
    }

    /**
     * Get cart menu items from cart model
     */
    // FIXME creezi un nou database, mai sus il dai ca parametru, si mai initializezi altul prin constructor
    // FIXME    shopdatabase nu ar trebui legat de model - vezi design pattern DAO (database access object)
    public ArrayList<String> getCartMenuItems() {
        return new ShopDataBase().getCartMenuItems();
    }
}
