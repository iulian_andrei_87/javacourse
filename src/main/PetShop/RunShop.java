package main.PetShop;

import main.PetShop.Controllers.CartController;
import main.PetShop.Controllers.ProductController;
import main.PetShop.Models.Cart;
import main.PetShop.Models.ShopDataBase;
import main.PetShop.Views.MenuView;

/**
 * Class runs the pet shop program
 */
// FIXME incearca sa aplici ce am facut data trecuta, la modul new Aplicatie().start(); ca sa scapi de toate chestiile statice
// FIXME    care nu au neaparat sens ca sunt statice, ci doar le folosesti pentru main
// FIXME    poate in cazul in care lasi cu main, nici nu are sens sa ai campuri, ci doar niste variabile in metoda
public class RunShop {

    /** Class variable for the shop database */
    private static ShopDataBase dataBase = new ShopDataBase();

    /** Class variable for the cart model */
    private static Cart cart = new Cart();

    /** Class variable for the product controller */
    private static ProductController productController = new ProductController(dataBase, cart);

    /** Class variable for the cart controller */
    private static CartController cartController = new CartController(dataBase, cart);

    private static final MenuView MENU_VIEW = new MenuView("Main menu", dataBase.getMenuItems());

    /** Constant that is used as universal return to previous menu */
    public static final int RETURN_TO_MENU = 0;

    public static void main(String[] args) {
        // initialize products from db
        dataBase.initializeProducts();

        // set flag
        boolean flag = true;

        do {
            // show main menu
            MENU_VIEW.showView();

            // grab first selection from user
            int selection = productController.selection();

            switch (selection) {
                case RETURN_TO_MENU:
                    flag = false;
                    break;
                case 1:
                    productController.runController();
                    break;
                case 2:
                    cartController.runController();
                    break;
                case 3:
                    System.out.println("\n" +
                            "We have Dragons!!!");
                    break;
            }
        } while (flag);
    }
}